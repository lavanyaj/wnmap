char * VERSIOWN;

#include "estruc.hh"
#include "funaux.hh"
#include "hypepo.hh"
#include "restric.hh"
#include "param.hh"


#define AA  0x1         //     1   aa
#define IA  0x2         //     2   ia
#define AI  0x4         //     4   ai
#define II  0x8         //     8   ii
#define WS  0x10        //    16   words
#define GL  0x20        //    32   gloss
#define FR  0x40        //    64   frames
#define AN  0x80        //   128   antonims
#define ST  0x100       //   256   Similar to
#define AS  0x200       //   512   Also see
#define PN  0x400       //  1024   Pertains to noun
#define AT  0x800       //  2048   Attribute of noun
#define PV  0x1000      //  4096   Participe of verb
#define DA  0x2000      //  8192   Derived from adjective
#define HM  0x4000      // 16384   Holonym Meronym
#define EC  0x8000      // 32768   Entailment & Cause for verbs


void CercarConexions(Taxonomia &);
void Conectar(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void Dobles(Taxonomia &, int);

list<void *> rest;

void         aa(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void         ia(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void         ai(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void         ii(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void        paa(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void      words(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void      gloss(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void     frames(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void   antonims(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void    similar(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void    alsosee(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void   pertains(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void  attribute(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void participle(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void    derived(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void    hol_mer(Taxonomia &, node, LlistaEtiq &, Etiqueta &);
void    ent_cau(Taxonomia &, node, LlistaEtiq &, Etiqueta &);


void restriccions(Taxonomia & tax)
//Modifica la taxonomia creant les conexions segons les restriccions que usem.
{
  int r=0;

   if(AA & TREST) {r = 3;
                   if(PROFUN) rest.append((void *) paa);
		   else rest.append((void *) aa); 
                  }
   if(IA & TREST) {rest.append((void *) ia); r = 6;}
   if(AI & TREST) {rest.append((void *) ai); r = 9;}
   if(II & TREST) {rest.append((void *) ii); r = 12;}
   if(WS & TREST) {rest.append((void *) words);}
   if(GL & TREST){
     rest.append((void *) gloss);
     cargarComuns();
   }
   if(FR & TREST) {rest.append((void *) frames);}
   if(AN & TREST || ST & TREST || AS & TREST || HM & TREST){
     CrearTaula(tax);
     if(AN & TREST) rest.append((void *) antonims);
     if(ST & TREST) rest.append((void *) similar);
     if(AS & TREST) rest.append((void *) alsosee);
     if(HM & TREST) rest.append((void *) hol_mer);
   }
   if(PN & TREST || AT & TREST){
     CrearTraslateNoun();
     if(PN & TREST) rest.append((void *) pertains);
     if(AT & TREST) rest.append((void *) attribute);
   }
   if(PV & TREST){CrearTraslateVerb(); rest.append((void *) participle);}
   if(DA & TREST){CrearTraslateAdj();  rest.append((void *) derived);}

   cout<< "Anem a conectar"<<endl;
   CercarConexions(tax);
   if(DOBLES) Dobles(tax, r);
}

/*************************************************************************
             RECORREGUT PER LA TAXONOMIA PER FER LES CONEXIONS
*************************************************************************/

void CercarConexions(Taxonomia & t)
{
  node n;
  list_item it;
  VERSIOWN = T_DEST;
  ObrirWordNet();

  forall_nodes(n, t){
    infonode & info = t[n];
    if(info.nn % 500 == 0)cout<<info.nn/100<<endl;
    LlistaEtiq & ll = info.ll;
    forall_items(it, ll) Conectar(t, n, ll, ll[it]);
  }
  TancarWordNet();
}

void Conectar(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e){
  void * pfr;
  void (* fr)(Taxonomia &, node, LlistaEtiq &, Etiqueta &);

  forall(pfr, rest){ 
    fr = (void (*)(Taxonomia &, node, LlistaEtiq &, Etiqueta &))pfr;
    fr(t, n, ll, e);
  }
}

/*************************************************************************
                            LES RESTRICCIONS
*************************************************************************/

void aa(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e){
  list<node_pes> lln;
  list<edge> le;
  list<int> lpose;
  list<syn_pes> lls;
  LlistaSynset lsp;
  syn_pes sp;
  list_item it, itep, its, ita;
  node p;
  Etiqueta ep;
  Conedge ce;
  Conexio c;
  direccio d;

  CercarHype(t, n, lln, le, lpose, 1.0, 1.0);
  CercarHypeWN(e.syn(), lls, 1.0, 1.0);
  DBG "lln es: "<<lln<<endl<<"lls es: "<<lls<<endl;

  forall_items(it, lln){
    DBG "Entrem amb: "<<lln[it]<<endl;
    p = lln[it].n;
    LlistaEtiq & llep = t[p].ll;
    forall_items(itep, llep){
      Etiqueta & ep = llep[itep];
      sp.syn = ep.syn();
      if((its = lls.search(sp))){
	c.buidar();
	c.pesconex = lls[its].dre * lln[it].dre * CONS[1];
	c.tipus = CapDalt;
	d.dn = p; d.posEtiq = llep.rank(ep)-1; d.posNode = t[p].nn;
	d.lle = lln[it].ares;
	d.poslle = lln[it].posares;
	c.conectar(d);
	e.conectar(c);

	c.buidar();
	c.pesconex = lls[its].dro * lln[it].dro * CONS[2];
	c.tipus = CapBaix;
	d.dn = n; d.posEtiq = ll.rank(e)-1; d.posNode = t[n].nn;
	d.lle = lln[it].ares;
	d.poslle = lln[it].posares;
	c.conectar(d);
	ep.conectar(c);
    
	ce.np = p;
	ce.posnp = t[p].nn;
	ce.nf = n;
	ce.posnf = t[n].nn;
	ce.etp = llep.rank(ep)-1;
	ce.etf = ll.rank(e)-1;
	ce.pes = lln[it].dre * CONS[1];
	forall_items(ita, lln[it].ares) t[lln[it].ares[ita]].llcon.append(ce);
      }
    }
  }

  // Pel nombre de fills:
  switch(FILLS){
  case 1:zeroFills(t, n, e); break;
  case 2:igualFills(t, n, e); break;
  case 3:diferenciaFills(t, n, e); break;
  }
  // Si el dos synsets tenen igual nombre de fills (19/12/2002)
  /*
  lsp.hypo(e.syn());
  if(lsp.length() == t.indeg(n)){
    c.buidar();
    c.pesconex = RENF;
    c.tipus = Altres;
    e.conectar(c);
  }
  */
  // a m�s diferencia del nombre de fills menys pes (3/10/2003)
  /*
  float diff;
  lsp.hypo(e.syn());
  diff = (lsp.length()+0.1)/(t.indeg(n)+0.1);
  if(diff > 1)diff = 1/diff;
  diff = pow(diff, 0.5);
  c.buidar();
  c.pesconex = RENF * diff;
  c.tipus = Altres;
  e.conectar(c);
  */
}

void ia(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e){
  list<syn_pes> lls;
  syn_pes sp;
  list_item itp, its;
  edge ed;
  node p;
  Conexio c;
  direccio d;
  Conedge ce;

  CercarHypeWN(e.syn(), lls, 1.0, 1.0);

    forall_out_edges(ed, n)
    {
      p = t.target(ed);
      LlistaEtiq & llep = t[p].ll;
      forall_items(itp, llep){
	Etiqueta & ep = llep[itp];
	sp.syn = ep.syn();
	if((its = lls.search(sp))){
	  c.buidar();
	  c.pesconex = lls[its].dre * CONS[4];
	  c.tipus = CapDalt;
	  d.dn = p; d.posEtiq = llep.rank(ep)-1; d.posNode = t[p].nn;
 	  d.lle.append(ed);
	  d.poslle.append(t[ed].ne);
	  c.conectar(d);
	  e.conectar(c);

	  c.buidar();
	  c.pesconex = lls[its].dro * CONS[5];
	  c.tipus = CapBaix;
	  d.dn = n; d.posEtiq = ll.rank(e)-1; d.posNode = t[n].nn;
	  d.lle.append(ed);
	  d.poslle.append(t[ed].ne);
	  c.conectar(d);
	  ep.conectar(c);

	  ce.np = p;
	  ce.posnp = t[p].nn;
	  ce.nf = n;
	  ce.posnf = t[n].nn;
	  ce.etp = llep.rank(llep[itp])-1;
	  ce.etf = ll.rank(e)-1;
	  ce.pes = lls[its].dre * CONS[4];
	  t[ed].llcon.append(ce);
	}
      }
    }

  // Pel nombre de fills:
  switch(FILLS){
  case 1:zeroFills(t, n, e); break;
  case 2:igualFills(t, n, e); break;
  case 3:diferenciaFills(t, n, e); break;
  }
}


void ai(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  list<node_pes> lln;
  list<edge> le;
  list<int> lpose;
  list_item it, itep, ita;
  node p;
  LlistaSynset lsp;
  Etiqueta ep;
  Conedge ce;
  Conexio c;
  direccio d;

  lsp.hype(e.syn());
  CercarHype(t, n, lln, le, lpose, 1.0, 1.0);

  forall_items(it, lln){
    p = lln[it].n;
    LlistaEtiq & llep = t[p].ll;
    forall_items(itep, llep){
      Etiqueta & ep = llep[itep];
      if(lsp.search(ep.syn())){
	c.buidar();
	c.pesconex = lln[it].dre * CONS[7];
	c.tipus = CapDalt;
	d.dn = p; d.posEtiq = llep.rank(ep)-1; d.posNode = t[p].nn;
	d.lle = lln[it].ares;
	d.poslle = lln[it].posares;
	c.conectar(d);
	e.conectar(c);

	c.buidar();
	c.pesconex = lln[it].dro * CONS[8];
	c.tipus = CapBaix;
	d.dn = n; d.posEtiq = ll.rank(e)-1; d.posNode = t[n].nn;
	d.lle = lln[it].ares;
	d.poslle = lln[it].posares;
	c.conectar(d);
	ep.conectar(c);
    
	ce.np = p;
	ce.posnp = t[p].nn;
	ce.nf = n;
	ce.posnf = t[n].nn;
	ce.etp = llep.rank(ep)-1;
	ce.etf = ll.rank(e)-1;
	ce.pes = lln[it].dre * CONS[7];
	forall_items(ita, lln[it].ares) t[lln[it].ares[ita]].llcon.append(ce);
      }
    }
  }

  // Pel nombre de fills:
  switch(FILLS){
  case 1:zeroFills(t, n, e); break;
  case 2:igualFills(t, n, e); break;
  case 3:diferenciaFills(t, n, e); break;
  }
}

void ii(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e){
  node p;
  edge ed;
  list_item itp;
  LlistaSynset lsp;
  Conexio c;
  direccio d;
  Conedge ce;
  
  lsp.hype(e.syn());
  
  forall_out_edges(ed, n)
    {
      p = t.target(ed);
      LlistaEtiq & llep = t[p].ll;
      forall_items(itp, llep){
	if(lsp.search(llep[itp].syn())){
	  c.pesconex = 1*CONS[10];
	  c.tipus = CapDalt;
	  d.dn = p; d.posEtiq = llep.rank(llep[itp])-1; d.posNode = t[p].nn;
	  d.lle.append(ed);
	  d.poslle.append(t[ed].ne);
          c.buidar();
	  c.conectar(d);
	  e.conectar(c);
	  
	  c.buidar();
	  c.pesconex = CONS[11];
	  c.tipus = CapBaix;
	  d.dn = n; d.posEtiq = ll.rank(e)-1; d.posNode = t[n].nn;
	  d.lle.append(ed);
	  d.poslle.append(t[ed].ne);
	  c.conectar(d);
	  llep[itp].conectar(c);
	  
	  ce.np = p;
	  ce.posnp = t[p].nn;
	  ce.nf = n;
	  ce.posnf = t[n].nn;
	  ce.etp = llep.rank(llep[itp])-1;
	  ce.etf = ll.rank(e)-1;
	  ce.pes = 1*CONS[10];
	  t[ed].llcon.append(ce);
	}
      }
    }

  // Pel nombre de fills:
  switch(FILLS){
  case 1:zeroFills(t, n, e); break;
  case 2:igualFills(t, n, e); break;
  case 3:diferenciaFills(t, n, e); break;
  }
  /*
  // Si el dos synsets tenen igual nombre de fills (19/12/2002)
  lsp.hypo(e.syn());
  if(lsp.length() == t.indeg(n)){
    c.buidar();
    c.pesconex = RENF;
    c.tipus = Altres;
    e.conectar(c);
  }
  */
  /* Si el dos synsets tenen 0 fills
     if(t.indeg(n) == 0){
     lsp.hypo(e.syn());
     if(lsp.empty()){
     c.buidar();
     c.pesconex = CONS[11]*0.1;
     c.tipus = Altres;
     e.conectar(c);
     }
     } */
  /*
  // a m�s diferencia del nombre de fills menys pes (3/10/2003)
  float diff;
  lsp.hypo(e.syn());
  diff = (lsp.length()+0.1)/(t.indeg(n)+0.1);
  if(diff > 1)diff = 1/diff;
  diff = pow(diff, 0.5);
  c.buidar();
  c.pesconex = RENF * diff;
  c.tipus = Altres;
  e.conectar(c);
  */
}

void paa(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  list<node_pes> lln;
  list<syn_pes> lls;
  list<edge> le;
  list<int> lpose;
  list<coincideix> llcoi;
  LlistaSynset lsp;
  coincideix coi;
  syn_pes sp;
  list_item it, itep, itef, its;
  node p, f;
  Conexio c;
  direccio d;

  VERSIOWN = T_DEST;
  DBG "\nObrim 1.6";

  CercarHype(t, n, lln, le, lpose, 1.0, 1.0);
  CercarHypeWN(e.syn(), lls, 1.0, 1.0);
  DBG "\nFi TOTAL WN";
  forall_items(it, lln){
    p = lln[it].n;
    LlistaEtiq & llep = t[p].ll;
    forall_items(itep, llep){
      Etiqueta & ep = llep[itep];
      sp.syn = ep.syn();
      if((its = lls.search(sp))){
	coi.n = p;
	coi.posEtiq = llep.rank(ep)-1;
	coi.dr = lln[it].dr * lls[its].dr * CONS[1];
	llcoi.append(coi);
      }
    }
  }
  DBG "\nCoincidencies: "<<llcoi<<endl;
  llcoi.sort();
  llcoi.reverse();

  int profun = 0;
  float PesAct;
  list_item itcoi = llcoi.first();
  DBG "First: "<<itcoi<<endl;
  if(itcoi){
     coi = llcoi[itcoi];
     PesAct = coi.dr;
  }
  while(itcoi && profun < PROFUN){
      c.buidar();
      c.pesconex = coi.dr;
      c.tipus = CapDalt;
      d.dn = coi.n; d.posEtiq = coi.posEtiq; d.posNode = t[coi.n].nn;
      c.conectar(d);
      e.conectar(c);

      itcoi = llcoi.succ(itcoi);
      if(itcoi){
        coi = llcoi[itcoi];
        if(llcoi[itcoi].dr != PesAct){
	   profun++;
	   PesAct = coi.dr;
	}
      }
  }

  le.clear();
  lpose.clear();
  //Ara anem pels hiponims.
  lln.clear(); lls.clear(); llcoi.clear();
  CercarHypo(t, n, lln, 1.0);
  CercarHypoWN(e.syn(), lls, 1.0);
  forall_items(it, lln){
    f = lln[it].n;
    LlistaEtiq & llef = t[f].ll;
    forall_items(itef, llef){
      Etiqueta & ef = llef[itef];
      sp.syn = ef.syn();
      if((its = lls.search(sp))){
	coi.n = f;
	coi.posEtiq = llef.rank(ef)-1;
	coi.dr = lln[it].dr * lls[its].dr * CONS[2];
	llcoi.append(coi);
      }
    }
  }

  llcoi.sort();
  llcoi.reverse();
  profun = 0;
  itcoi = llcoi.first();
  if(itcoi){
     coi = llcoi[itcoi];
     PesAct = coi.dr;
  }
  while(itcoi && profun < PROFUN){
    c.buidar();
    c.pesconex = coi.dr;
    c.tipus = CapBaix;
    d.dn = coi.n; d.posEtiq = coi.posEtiq; d.posNode = t[coi.n].nn;
    c.conectar(d);
    e.conectar(c);

    itcoi = llcoi.succ(itcoi);
    if(itcoi){
      coi = llcoi[itcoi];
      if(llcoi[itcoi].dr != PesAct){
        profun++;
	PesAct = coi.dr;
      }
    }
  }

  // Pel nombre de fills:
  switch(FILLS){
  case 1:zeroFills(t, n, e); break;
  case 2:igualFills(t, n, e); break;
  case 3:diferenciaFills(t, n, e); break;
  }
  /*
  // Si el dos synsets tenen igual nombre de fills (19/12/2002)
  lsp.hypo(e.syn());
  if(lsp.length() == t.indeg(n)){
    c.buidar();
    c.pesconex = RENF;
    c.tipus = Altres;
    e.conectar(c);
  }
  */
  // a m�s diferencia del nombre de fills menys pes (3/10/2003)
  /*
  float diff;
  lsp.hypo(e.syn());
  diff = (lsp.length()+0.1)/(t.indeg(n)+0.1);
  if(diff > 1)diff = 1/diff;
  diff = pow(diff, 0.5);
  c.buidar();
  c.pesconex = RENF * diff;
  c.tipus = Altres;
  e.conectar(c);
  */
}



void Dobles(Taxonomia & t, int r){
  node n;
  list_item it, itDalt, itBaix, itd;
  list<Conexio> lCapDalt, lCapBaix;
  Conexio c;
  direccio d;
  float pD, pB;

  forall_nodes(n, t){
    infonode & i = t[n];
    LlistaEtiq & ll = i.ll;
    forall_items(it, ll){
      Etiqueta & e = ll[it];
      list<Conexio> & llc = e.AccesConex();
      lCapDalt.clear(); lCapBaix.clear();
      forall(c, llc){
	if(c.tipus == CapDalt) lCapDalt.append(c);
	if(c.tipus == CapBaix) lCapBaix.append(c);
      }
      lCapDalt.sort(); lCapBaix.sort();
       //introduit el 14/05/03 per agafar els primers de m�xim pes.
      pD = pB = 0;
       itDalt = lCapDalt.first();
      if(itDalt) pD = lCapDalt[itDalt].pesconex;
      itBaix = lCapBaix.first(); 
      if(itBaix) pB = lCapBaix[itBaix].pesconex;
      forall_items(itDalt, lCapDalt)
	if(lCapDalt[itDalt].pesconex < pD)lCapDalt.del(itDalt);
      forall_items(itBaix, lCapBaix)
	if(lCapBaix[itBaix].pesconex < pB)lCapBaix.del(itBaix);
      //      forall_items(itDalt, lCapDalt)
      //	forall_items(itBaix, lCapBaix){
      // Nom�s fem doble el primer amb el primer, ja que si els fem
      // tots amb tots la cantitat de mem�ria necessari �s excessiva (22/1/03).
      //      itDalt = lCapDalt.first();
      //      itBaix = lCapBaix.first();
      //      if(itDalt && itBaix){  //error en 05284499 (13/05/03)
      forall_items(itDalt, lCapDalt)
	forall_items(itBaix, lCapBaix){ 
	  c.buidar();
	  c.pesconex = CONS[r] * lCapDalt[itDalt].pesconex * 
	               lCapBaix[itBaix].pesconex;
	  c.tipus = Ambdos;
	  itd = lCapDalt[itDalt].llptre.first();
	  d = lCapDalt[itDalt].llptre[itd];
	  c.conectar(d);
	  itd = lCapBaix[itBaix].llptre.first();
	  d = lCapBaix[itBaix].llptre[itd];
	  c.conectar(d);
	  e.conectar(c);
      }
    }
  }
}


void words(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e){
  long syn15, syn16;
  LlistaWords ws15, ws16;
  Conexio c;

  char & a = t[n].info.sp[0];
  char * p = &a;
  syn15 = atol(p);
  syn16 = e.syn();

  VERSIOWN = T_ORIG;
  ws15.words(syn15);

  VERSIOWN = T_DEST;
  ws16.words(syn16);

  c.buidar();
  c.tipus = Altres;
  if(FWG){
    int a, f;
    a =  ws16.Coincideix(ws15);
    f = ws16.length() + ws15.length() - 2*a;
    if(a+f != 0)
      c.pesconex = pow(a, 3)/pow(a+f, 2);
    else c.pesconex = 0.5;
  }else c.pesconex = ws16.Coincideix(ws15);
  e.conectar(c);
}


void gloss(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  long syn15, syn16;
  LlistaWords gls15, gls16;
  Conexio c;

  char & a = t[n].info.sp[0];
  char * p = &a;
  syn15 = atol(p);
  syn16 = e.syn();

  VERSIOWN = T_ORIG;
  gls15.gloss(syn15);
  gls15.EliminaComuns();

  VERSIOWN = T_DEST;
  gls16.gloss(syn16);
  gls16.EliminaComuns();

  c.buidar();
  c.tipus = Altres;
  if(FWG){
    int a, f;
    a =  gls16.Coincideix(gls15);
    f = gls16.length() + gls15.length() - 2*a;
    if(a+f != 0)
      c.pesconex = pow(a, 3)/pow(a+f, 2);
    else c.pesconex = 0.5;
  }else  c.pesconex = gls16.Coincideix(gls15);
  e.conectar(c);
}

void frames(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e){
  long syn15, syn16;
  LlistaFrame fr15, fr16;
  Conexio c;
  
  char & a = t[n].info.sp[0];
  char * p = &a;
  syn15 = atol(p);
  syn16 = e.syn();

  VERSIOWN = T_ORIG;
  fr15.frame(syn15);
  VERSIOWN = T_DEST;
  fr16.frame(syn16);

  c.buidar();
  c.tipus = Altres;
  if(FWG){
    int a, f;
    a =  fr16.Coincideix(fr15);
    f = fr16.length() + fr15.length() - 2*a;
    if(a+f != 0)
      c.pesconex = pow(a, 3)/pow(a+f, 2);
    else c.pesconex = 0.5;
  }else c.pesconex = fr16.Coincideix(fr15);
  e.conectar(c);
}

void antonims(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e){
  node na;
  char * p;
  long ea;
  list_item ita, itea;
  LlistaSynset lsa5, lsa6;
  Conexio c;
  direccio d;
  
  lsa6.anto(e.syn());
  VERSIOWN = T_ORIG;
  char & a = t[n].info.sp[0];
  p = &a;
  lsa5.anto(atol(p));
  VERSIOWN = T_DEST;
  
  forall_items(itea, lsa5){
    ea = lsa5[itea];
    na = TAULATAX[ea];
    LlistaEtiq & llea = t[na].ll;
    forall_items(ita, llea){
      if(lsa6.search(llea[ita].syn())){
	c.pesconex = RA;
	c.tipus = Altres;
	d.dn = na; d.posEtiq = llea.rank(llea[ita])-1; d.posNode = t[na].nn;
	c.buidar();
	c.conectar(d);
	e.conectar(c);
      }
    }
  }
}


void similar(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  node na;
  long ea;
  list_item ita, itea;
  LlistaSynset lsa5, lsa6;
  Conexio c;
  direccio d;

  lsa6.simi(e.syn());
  VERSIOWN = T_ORIG;
  char & a = t[n].info.sp[0];
  char * p = &a;
  lsa5.simi(atol(p));
  VERSIOWN =T_DEST;

  forall_items(itea, lsa5){
    ea = lsa5[itea];
    na = TAULATAX[ea];
    LlistaEtiq & llea = t[na].ll;
    forall_items(ita, llea){
      if(lsa6.search(llea[ita].syn())){
	  c.pesconex = RA;
	  c.tipus = Altres;
	  d.dn = na; d.posEtiq = llea.rank(llea[ita])-1; d.posNode = t[na].nn;
          c.buidar();
	  c.conectar(d);
	  e.conectar(c);
      }
   }
  }
}


void alsosee(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  node na;
  long ea;
  list_item ita, itea;
  LlistaSynset lsa5, lsa6;
  Conexio c;
  direccio d;

  lsa6.alse(e.syn());
  VERSIOWN = T_ORIG;
  char & a = t[n].info.sp[0];
  char * p = &a;
  lsa5.alse(atol(p));
  VERSIOWN =T_DEST;

  forall_items(itea, lsa5){
    ea = lsa5[itea];
    na = TAULATAX[ea];
    LlistaEtiq & llea = t[na].ll;
    forall_items(ita, llea){
      if(lsa6.search(llea[ita].syn())){
	  c.pesconex = RA;
	  c.tipus = Altres;
	  d.dn = na; d.posEtiq = llea.rank(llea[ita])-1; d.posNode = t[na].nn;
          c.buidar();
	  c.conectar(d);
	  e.conectar(c);
      }
   }
  }
}

void pertains(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  LlistaSynset ls5, ls6;
  list_item it5, itn6;
  long n5;
  list<itemTrasl> ln6;
  float suma = 0.0;
  Conexio c;

  ls6.pert(e.syn());
  VERSIOWN = T_ORIG;
  char & a = t[n].info.sp[0];
  char * p = &a;
  ls5.pert(atol(p));

  forall_items(it5, ls5){
    n5 = ls5[it5];
    ln6 = TRASLNOUN[n5];
    forall_items(itn6, ln6)
      if(ls6.search(ln6[itn6].syn))
	suma += ln6[itn6].pes;
  }
  c.buidar();
  c.tipus = Altres;
  c.pesconex = suma;
  e.conectar(c);
  VERSIOWN = T_DEST;
}


void attribute(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  LlistaSynset ls5, ls6;
  list_item it5, itn6;
  long n5;
  list<itemTrasl> ln6;
  float suma = 0.0;
  Conexio c;

  ls6.attr(e.syn());
  VERSIOWN = T_ORIG;
  char & a = t[n].info.sp[0];
  char * p = &a;
  ls5.attr(atol(p));

  forall_items(it5, ls5){
    n5 = ls5[it5];
    ln6 = TRASLNOUN[n5];
    forall_items(itn6, ln6)
      if(ls6.search(ln6[itn6].syn))
	suma += ln6[itn6].pes;
  }
  c.buidar();
  c.tipus = Altres;
  c.pesconex = suma;
  e.conectar(c);
  VERSIOWN = T_DEST;
}


void participle(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  LlistaSynset ls5, ls6;
  list_item it5, itn6;
  long n5;
  list<itemTrasl> ln6;
  float suma = 0.0;
  Conexio c;

  ls6.parv(e.syn());
  VERSIOWN = T_ORIG;
  char & a = t[n].info.sp[0];
  char * p = &a;
  ls5.parv(atol(p));

  forall_items(it5, ls5){
    n5 = ls5[it5];
    ln6 = TRASLVERB[n5];
    forall_items(itn6, ln6)
      if(ls6.search(ln6[itn6].syn))
	suma += ln6[itn6].pes;
  }
  c.buidar();
  c.tipus = Altres;
  c.pesconex = suma;
  e.conectar(c);
  VERSIOWN = T_DEST;
}


void derived(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e)
{
  LlistaSynset ls5, ls6;
  list_item it5, itn6;
  long n5;
  list<itemTrasl> ln6;
  float suma = 0.0;
  Conexio c;

  ls6.pert(e.syn());
  VERSIOWN = T_ORIG;
  char & a = t[n].info.sp[0];
  char * p = &a;
  ls5.pert(atol(p));

  forall_items(it5, ls5){
    n5 = ls5[it5];
    ln6 = TRASLADJ[n5];
    forall_items(itn6, ln6)
      if(ls6.search(ln6[itn6].syn))
	suma += ln6[itn6].pes;
  }
  c.buidar();
  c.tipus = Altres;
  c.pesconex = suma;
  e.conectar(c);
  VERSIOWN = T_DEST;
}

void hol_mer(Taxonomia & t, node n, LlistaEtiq & ll, Etiqueta & e){
  node na;
  char * p;
  long ea;
  list_item ita, itea;
  LlistaSynset lsa5, lsa6;
  Conexio c;
  direccio d;
  
  lsa6.h_m(e.syn());
  VERSIOWN = T_ORIG;
  char & a = t[n].info.sp[0];
  p = &a;
  lsa5.h_m(atol(p));
  VERSIOWN = T_DEST;
  
  forall_items(itea, lsa5){
    ea = lsa5[itea];
    na = TAULATAX[ea];
    LlistaEtiq & llea = t[na].ll;
    forall_items(ita, llea){
      if(lsa6.search(llea[ita].syn())){
	c.pesconex = RHM;
	c.tipus = Altres;
	d.dn = na; d.posEtiq = llea.rank(llea[ita])-1; d.posNode = t[na].nn;
	c.buidar();
	c.conectar(d);
	e.conectar(c);
      }
    }
  }
}

void ent_cau(Taxonomia &, node, LlistaEtiq &, Etiqueta &){
}







