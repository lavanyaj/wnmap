#include "hypepo.hh"
#include <string.h>

using std::filebuf;

FILE *fOrig, *fDest;
LlistaWords COMU;

/*****************************************************************************
***      Implentació de les funcions associades al TAD LlistaSynset        ***
*****************************************************************************/

void LlistaSynset::hype(long syn, int pos)
{
  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");
  DBG1 "ll1 "<<endl;
  for(int i=0; i< ptr->ptrcount; i++){
   if(ptr->ptrtyp[i] == 2) this->append(ptr->ptroff[i]);
  }
  DBG1 "ll2 "<<endl;
  free_synset(ptr);
}


void LlistaSynset::hypo(long syn, int pos)
{

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 3) this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::anto(long syn, int pos)
{

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 1) this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::simi(long syn, int pos)
{

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 5) this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::alse(long syn, int pos)
{

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 16) this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::pert(long syn, int pos)
{

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 17) this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::parv(long syn, int pos){

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 15) this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::attr(long syn, int pos){

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 18) this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::h_m(long syn, int pos){

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] >= 6 && ptr->ptrtyp[i] <= 13)
      this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::entail(long syn, int pos){

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 4)
      this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

void LlistaSynset::cause(long syn, int pos){

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");

  for(int i=0; i< ptr->ptrcount; i++)
   if(ptr->ptrtyp[i] == 14)
      this->append(ptr->ptroff[i]);
  free_synset(ptr);
}

/************************************************************************************
***          Implentació de les funcions associades al TAD LlistaWords           ***
************************************************************************************/

void LlistaWords::words(long syn, int pos)
{
  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");
  for(int i = 0; i< ptr->wcount; i++) this->append(ptr->words[i]);
  free_synset(ptr);
}


void LlistaWords::gloss(long syn, int pos){
  char * nulls = " !\"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\n\0";
  char * mot;

  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");
  mot = strtok(ptr->defn, nulls);
  while(mot != NULL){
    this->append(mot);
    mot = strtok(NULL, nulls);
  }
  free_synset(ptr);
}

int LlistaWords::Coincideix(LlistaWords llw)
{
  int c = 0;
  string s;

  forall(s, llw)
    if(this->search(s))c++;
  return c;
}

void cargarComuns(){
  filebuf bufcomu;
  char file[] = "empty";
 
  if (!bufcomu.open(file, ios::in))
    error ("no es pot obrir el fitxer ", file);
  istream iscomu(&bufcomu);
 
  COMU.read(iscomu);
}

void LlistaWords::EliminaComuns(){
  string c;
  forall(c, COMU)this->remove(c);
}

void LlistaFrame::frame(long syn, int pos)
{
  this->clear();
  SynsetPtr ptr = llegir_synset(pos, syn, "");
  for(int i = 0; i< ptr->fcount; i++) this->append(ptr->frmid[i]);
  free_synset(ptr);
}

int LlistaFrame::Coincideix(LlistaFrame llf)
{
  int f, c = 0;

  forall(f, llf)
    if(this->search(f))c++;
  return c;
}

leda::string posString(int pos){
  switch(pos){
  case 1: return "noun";
  case 2: return "verb";
  case 3: return "adj";
  case 4: return "adv";
  }
  return "";
}

void ObrirWordNet(){
  string file;

  file = (string)PREFIXWN + T_ORIG + "/dict/data." + posString();
  fOrig = fopen(file, "r");
  DBG1 "fileO = "<<file<<endl;
  file = (string)PREFIXWN + T_DEST + "/dict/data." + posString();
  DBG1 "fileD = "<<file<<endl;
  fDest = fopen(file, "r");
}

void TancarWordNet(){

  fclose(fOrig);
  fclose(fDest);
}

FILE * versio(){
  if(!strcmp(VERSIOWN, T_ORIG))
    return fOrig;
  else return fDest;
}

SynsetPtr llegir_synset(int p, long syn, char * w){
  fseek(versio(), syn, SEEK_SET);
  return parse_synset(versio(), p, w);
}


