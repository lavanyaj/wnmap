#include "estruc.hh"
#include "funaux.hh"
#include "param.hh"
#include <string.h>

using namespace std;

void restriccions(Taxonomia &);
void ComprovarArguments(int, char **, char *, char *);

int main( int argc, char *argv[]){
  char fileparm[80] = {'\0'}, filegraf[80] = {'\0'};
  system("date");
  ComprovarArguments(argc, argv, fileparm, filegraf);

  ValorsDefecte();
  if(strcmp(fileparm, "")) LlegirConstans(fileparm);
  
  cout<<"Anem a crear la taxonomia"<<endl;
  Taxonomia tax;
  tax.llegir();

  cout<<"Anem a muntar les restriccions"<<endl;
  restriccions(tax);
  tax.normal();
  if(ONLYGRAF){
    filebuf bufgraf;
    if (!bufgraf.open(filegraf, ios::out))
      error ("no es pot obrir el fitxer ", filegraf);
    ostream osgraf(&bufgraf);
    osgraf << 0 << endl;
    osgraf << tax;
    bufgraf.close();
    exit(3);
  }

  cout<<"Anem a relaxar"<<endl;
  tax.relax(filegraf);

  tax.escriu();
  tax.comptar();
  system("date");
  return(0);
}


void ComprovarArguments(int argc, char *argv[], char * fp, char * fg)
{
  for(int i = 1; i < argc; i += 2){
    if(!strcmp(argv[i], "-p")) strcpy(fp, argv[i+1]);
    if(!strcmp(argv[i], "-g")) strcpy(fg, argv[i+1]);
  }
  cout <<"fileparm: "<<fp<<endl<<"filegraf: "<<fg<<endl;
}








