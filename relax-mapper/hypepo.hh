//////////////////////////////////////////////////////////////////////////////////////
//                     Definici� de TAD's i acc�s a WordNet                         //
//////////////////////////////////////////////////////////////////////////////////////

#ifndef _HYPEPO_HH
#define _HYPEPO_HH


extern "C" {
#include <stdio.h>
#include "wn.h"
#include "wnconsts.h"
}
#include <iostream.h>
#include <LEDA/core/list.h>
#include "funaux.hh"

using leda::list;


SynsetPtr llegir_synset(int, long, char *);
leda::string posString(int pos = POS);

//////////////////////////////////////////////////////////////////////////////////////
//                                  TAD's per WordNet                               //
//////////////////////////////////////////////////////////////////////////////////////

class LlistaSynset : public list<long>
{
public:
  void hype(long, int pos = POS);  //Ompla la llista del hypernyms del synset passat
  void hypo(long, int pos = POS);  //Ompla la llista del hyponyms del synset passat
  void anto(long, int pos = POS);  //Ompla la llista de antinonims del synset passat
  void simi(long, int pos = POS);
  void alse(long, int pos = POS);
  void pert(long, int pos = POS);
  void parv(long, int pos = POS);
  void attr(long, int pos = POS);
  void dadj(long, int pos = POS);
  void  h_m(long, int pos = POS);
  void entail(long, int pos = POS);
  void cause(long, int pos = POS);
};

void cargarComuns();
class LlistaWords : public list<string>
{
public:
  void words(long, int pos = POS);  //Ompla una llista amb les paraules del synset
  void gloss(long, int pos = POS);  //Ompla una llista amb les paraules de la glossa
  int Coincideix(LlistaWords);
  void EliminaComuns();
};

class LlistaFrame : public list<int>
{
public:
  void frame(long, int pos = POS);
  int Coincideix(LlistaFrame);
};

void ObrirWordNet();
void TancarWordNet();

#endif








