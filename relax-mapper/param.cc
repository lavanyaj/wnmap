unsigned char DEBUG = 1;
bool ONLYGRAF = false;
float EPSILON, ZERO;
int NITE;
float CONS[18];
bool DOBLES = 1;
int HEURISTICA = 0;
bool RELAXGEN = 0, EFICIENT = 0;
int PROFUN;
int POS = 1;
int DU, RW, RG, RA, RHM, FILLS, RENF;
bool FWG = true;
bool EDGES = true;
long TREST = 8;
float LDRE = 0.1;
char * PREFIXWN = "/Users/lavya810/git/wnmap/WordNet/";
//char * PREFIXWN = "/usr/local/WordNet/";
char T_ORIG[40];
char T_DEST[40];

#include <iostream.h>
#include "estruc.hh"
#include "funaux.hh"

using std::filebuf;
using std::ios;
using std::cout;

string NOMCONS[] = {"EPSILON", "AAE", "AAO", "AAB", "IAE", "IAO", "IAB", "AIE", "AIO", "AIB", "IIE", "IIO", "IIB", "DRED", "DROD", "DREO", "DROO"};

/*************************************************************************
                      LECTURA DE LES CONSTANTS
*************************************************************************/

void ValorsDefecte()
{
  EPSILON = CONS[0]=0.001;
  ZERO = 0.0000001;
  NITE = 100;
  PROFUN = 0;
  for(int i=1; i<13; i++) CONS[i]= 4*i;
  for(int i=13; i<17; i++) CONS[i]= 0.95;
  DU = 1;
  RW = RG = RA = RHM = RENF = 4;
  FILLS = 0;
}


void LlegirConstans(char * filecons){
  char rest[80];
  filebuf bufcons;

  if (!bufcons.open(filecons, ios::in))
    error ("no es pot obrir el fitxer ", filecons);
  istream is(&bufcons);


  while( !is.eof()){
    is.getline(rest, 80, ' ');

    if(!strcmp(rest, "EPSILON"))
       { is.getline(rest, 80, '\n'); EPSILON = CONS[0]=atof(rest);}

    if(!strcmp(rest, "ZERO"))
       { is.getline(rest, 80, '\n'); ZERO = atof(rest);}

    if(!strcmp(rest, "NITE"))
      // Nombre de iteracions del algorisme de relaxasi�
       { is.getline(rest, 80, '\n'); NITE =atoi(rest);}

    else if(!strcmp(rest, "AAE"))
       { is.getline(rest, 80, '\n'); CONS[1]=atof(rest);}

    else if(!strcmp(rest, "AAO"))
       { is.getline(rest, 80, '\n'); CONS[2]=atof(rest);}

    else if(!strcmp(rest, "AAB"))
       { is.getline(rest, 80, '\n'); CONS[3]=atof(rest);}

    else if(!strcmp(rest, "IAE"))
       { is.getline(rest, 80, '\n'); CONS[4]=atof(rest);}

    else if(!strcmp(rest, "IAO"))
       { is.getline(rest, 80, '\n'); CONS[5]=atof(rest);}

    else if(!strcmp(rest, "IAB"))
       { is.getline(rest, 80, '\n'); CONS[6]=atof(rest);}

    else if(!strcmp(rest, "AIE"))
       { is.getline(rest, 80, '\n'); CONS[7]=atof(rest);}

    else if(!strcmp(rest, "AIO"))
       { is.getline(rest, 80, '\n'); CONS[8]=atof(rest);}

    else if(!strcmp(rest, "AIB"))
       { is.getline(rest, 80, '\n'); CONS[9]=atof(rest);}

    else if(!strcmp(rest, "IIE"))
       { is.getline(rest, 80, '\n'); CONS[10]=atof(rest);}

    else if(!strcmp(rest, "IIO"))
       { is.getline(rest, 80, '\n'); CONS[11]=atof(rest);}

    else if(!strcmp(rest, "IIB"))
       { is.getline(rest, 80, '\n'); CONS[12]=atof(rest);}

    else if(!strcmp(rest, "DRED"))
      //Degradaci� Recursiva hipEronim en la taxonomia Dest�
       { is.getline(rest, 80, '\n'); CONS[13]=atof(rest);}

    else if(!strcmp(rest, "DROD"))
      //Degradaci� Recursiva hipOnims en la taxonomia Dest�
       { is.getline(rest, 80, '\n'); CONS[14]=atof(rest);}

    else if(!strcmp(rest, "DREO"))
      //Degradaci� Recursiva hipEronim en la taxonomia Origen
       { is.getline(rest, 80, '\n'); CONS[15]=atof(rest);}

    else if(!strcmp(rest, "LDRE"))
      //Limit de la degradaci� Recursiva hipEronim en la taxonomia Origen
      //per les restriccions del tipus a*
       { is.getline(rest, 80, '\n'); LDRE=atof(rest);}

    else if(!strcmp(rest, "DROO"))
      //Degradaci� Recursiva hipOnims en la taxonomia Origen
       { is.getline(rest, 80, '\n'); CONS[16]=atof(rest);}

    else if(!strcmp(rest, "DEBUG"))
       { is.getline(rest, 80, '\n'); DEBUG=(unsigned char)atoi(rest);}

    else if(!strcmp(rest, "ONLYGRAF"))
       { is.getline(rest, 80, '\n'); ONLYGRAF=(bool)atoi(rest);}

    else if(!strcmp(rest, "DOBLES"))
       { is.getline(rest, 80, '\n'); DOBLES=atoi(rest);}

    else if(!strcmp(rest, "HEURISTICA"))
       { is.getline(rest, 80, '\n'); HEURISTICA=atoi(rest);}

    else if(!strcmp(rest, "RELAXGEN"))
       { is.getline(rest, 80, '\n'); RELAXGEN=(bool)atoi(rest);}

    else if(!strcmp(rest, "EFICIENT"))
       { is.getline(rest, 80, '\n'); EFICIENT=(bool)atoi(rest);}

    else if(!strcmp(rest, "POS"))
       { is.getline(rest, 80, '\n'); POS=atoi(rest);}

    else if(!strcmp(rest, "PROFUN"))
       { is.getline(rest, 80, '\n'); PROFUN=atoi(rest);}

    else if(!strcmp(rest, "DU"))
       { is.getline(rest, 80, '\n'); DU=atoi(rest);}

    else if(!strcmp(rest, "RW"))
       { is.getline(rest, 80, '\n'); RW=atoi(rest);}

    else if(!strcmp(rest, "RG"))
       { is.getline(rest, 80, '\n'); RG=atoi(rest);}

    else if(!strcmp(rest, "RA"))
       { is.getline(rest, 80, '\n'); RA=atoi(rest);}

    else if(!strcmp(rest, "RHM"))
       { is.getline(rest, 80, '\n'); RHM=atoi(rest);}

    else if(!strcmp(rest, "FILLS"))
       { is.getline(rest, 80, '\n'); FILLS=atoi(rest);}

    else if(!strcmp(rest, "RENF"))
       { is.getline(rest, 80, '\n'); RENF=atoi(rest);}

    else if(!strcmp(rest, "EDGES"))
       { is.getline(rest, 80, '\n'); EDGES=(bool)atoi(rest);}

    else if(!strcmp(rest, "FWG"))
       { is.getline(rest, 80, '\n'); FWG=(bool)atoi(rest);}

    else if(!strcmp(rest, "TREST"))
       { is.getline(rest, 80, '\n'); TREST=atol(rest);}

    else if(!strcmp(rest, "T_ORIG"))
       { is.getline(rest, 80, '\n'); strcpy(T_ORIG,rest);}

    else if(!strcmp(rest, "T_DEST"))
       { is.getline(rest, 80, '\n'); strcpy(T_DEST,rest);}

    else  is.ignore(512, '\n');
  }
  bufcons.close();
  
      for(int i=0; i<17; i++) 
	cout <<"CONS["<<i<<"] ("<<NOMCONS[i]<<") = "<<CONS[i]<<endl;
      cout <<"DU = "<<DU<<endl
	   <<"RW = "<<RW<<endl
	   <<"RG = "<<RG<<endl
	   <<"RA = "<<RA<<endl
	   <<"RHM = "<<RHM<<endl
	   <<"FILLS = "<<FILLS<<endl
	   <<"RENF = "<<RENF<<endl
	   <<"LDRE = "<<LDRE<<endl
	   <<"EPSILON = "<<EPSILON<<endl
	   <<"ZERO = "<<ZERO<<endl
           <<"NITE = "<<NITE<<endl
           <<"HEURISTICA = "<<HEURISTICA<<endl
           <<"DOBLES = "<<DOBLES<<endl
           <<"RELAXGEN = "<<RELAXGEN<<endl
           <<"EFICIENT = "<<EFICIENT<<endl
           <<"ONLYGRAF = "<<ONLYGRAF<<endl
           <<"POS = "<<POS<<endl
	   <<"PROFUN = "<<PROFUN<<endl
	   <<"EDGES = "<<EDGES<<endl
	   <<"FWG = "<<FWG<<endl
	   <<"TREST = "<<TREST<<endl
	   <<"T_ORIG = "<<T_ORIG<<endl
	   <<"T_DEST = "<<T_DEST<<endl
	   <<"DEBUG = "<<(int)DEBUG<<endl;
}



