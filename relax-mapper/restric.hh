#ifndef _RESTRIC_HH
#define _RESTRIC_HH

#include "funaux.hh"

using std::filebuf;
using leda::list_item;

d_array<long, node> TAULATAX;

void CrearTaula(Taxonomia & t){
  node n;
  char * p;

  forall_nodes(n, t){
    char & a = t[n].info.sp[0];
    p = &a;
    TAULATAX[atol(p)] = n;
  }
}


class itemTrasl{
public:
       long syn;
       float pes;
       friend ostream& operator << (ostream& os, const itemTrasl &i){
	 os << i.syn <<" "<< i.pes<<" ";
	 return os;
};
       friend istream& operator >> (istream& is, itemTrasl &i){
	 is >> i.syn >> i.pes;
	 return is;
       }
};

d_array<long, list<itemTrasl> > TRASLNOUN;
d_array<long, list<itemTrasl> > TRASLVERB;
d_array<long, list<itemTrasl> > TRASLADJ;


void CrearTraslateNoun(){
  
  string filenoun = "/home/usuaris/daude/relax/res/"+
                    (string)T_ORIG+"-"+T_DEST+"/traslate.noun";

  filebuf bufnoun;

  if (!bufnoun.open(filenoun, ios::in ))
    error ("no es pot obrir el fitxer ", filenoun);
  istream isnoun(&bufnoun);

  long syn15;
  list<itemTrasl> ll16;
  isnoun >> syn15;
  while(!isnoun.eof()){
    ll16.read(isnoun, '\n');
    TRASLNOUN[syn15] = ll16;
    isnoun >> syn15;
  }
  bufnoun.close();
}

void CrearTraslateVerb(){
  string fileverb = "/home/usuaris/daude/relax/res/"+
                    (string)T_ORIG+"-"+T_DEST+"/traslate.verb";

  filebuf bufverb;

  if (!bufverb.open(fileverb, ios::in ))
    error ("no es pot obrir el fitxer ", fileverb);
  istream isverb(&bufverb);

  long syn15;
  list<itemTrasl> ll16;
    isverb >> syn15;
  while(!isverb.eof()){
    ll16.read(isverb, '\n');
    TRASLVERB[syn15] = ll16;
    isverb >> syn15;
  }
  bufverb.close();
}

void CrearTraslateAdj(){
  string fileadj = "/home/usuaris/daude/relax/res/"+
                   (string)T_ORIG+"-"+T_DEST+"/traslate.adj";

  filebuf bufadj;

  if (!bufadj.open(fileadj, ios::in))
    error ("no es pot obrir el fitxer ", fileadj);
  istream isadj(&bufadj);

  long syn15;
  list<itemTrasl> ll16;
    isadj >> syn15;
  while(!isadj.eof()){
    ll16.read(isadj, '\n');
    TRASLADJ[syn15] = ll16;
    isadj >> syn15;
  }
  bufadj.close();
}

d_array<long, list<infonode> > TaulaDestUnic;

void CrearTaulaDestUnic(Taxonomia & t){
  node n;
  list_item it;

  forall_nodes(n, t){
    infonode & i = t[n];
    LlistaEtiq & ll = i.ll;
    forall_items(it, ll){
      Etiqueta & e = ll[it];
      TaulaDestUnic[e.syn()].append(i);
    }
  }
  if(DEBUG){
    long s;
    cout << "Taula etiquetes"<<endl;
    forall_defined(s, TaulaDestUnic)cout<<s<<" "<<TaulaDestUnic[s]<<"\nf"<<s<<endl;
    cout << "Fi taula etiquetes"<<endl;
  }
}

class node_pes{
public:
      node n;
      float dr, dre, dro;
      list<edge> ares;
      list<int> posares;

  friend istream& operator >> (istream& is, node_pes &n){return is;};
  friend ostream& operator << (ostream& os, const node_pes &n){
    os<<n.n<<" "<<n.dre<<" "<<n.dro<<" // "<<n.ares<<endl;
    return os;};
  friend int compare(const node_pes & n1, const node_pes & n2){return leda::compare(n1.n, n2.n);};
  friend bool operator == (const node_pes & n1, const node_pes & n2){return n1.n==n2.n;};
};

void CercarHype(Taxonomia & t, node n, list<node_pes> & l, float dr)
{    //Encara no modificada per la llista d'arestes.
  edge ed;
  node p;
  node_pes ln;
  list_item it;

  forall_out_edges(ed, n)
  {
    p = t.target(ed);
    ln.n = p;
    ln.dr = dr;
    if((it = l.search(ln))){
      if(dr > l[it].dr){
	l[it].dr = dr;
	CercarHype(t, p, l, dr*CONS[15]);
      }
    }
    else{
      l.append(ln);
      CercarHype(t, p, l, dr*CONS[15]);
    }
  }
  
}



void CercarHype(Taxonomia& t, node n, list<node_pes> & l, list<edge> le, list<int> lpose, float dre, float dro)
{
  edge ed;
  node p;
  node_pes ln;
  list_item it;

  DBG t[n].info.sp<<" "<<t[n].info.acep<<"--->"<<le<<endl;
  if(dre > LDRE){
    forall_out_edges(ed, n)
      {
	p = t.target(ed);
	ln.n = p;
	ln.dre = dre; ln.dro = dro;
	le.append(ed);
	lpose.append(t[ed].ne);
	ln.ares = le;
	ln.posares = lpose;

	if((it = l.search(ln))){
	  if(dre > l[it].dre){
	    l[it].dre = dre;
	    l[it].dro = dro;
	    l[it].ares = ln.ares;
	    l[it].posares = ln.posares;
	    CercarHype(t, p, l, le, lpose, dre*CONS[15], dro*CONS[16]);
	  }
	  le.Pop();
	  lpose.Pop();
	}
	else{
	  l.append(ln);
	  CercarHype(t, p, l, le, lpose, dre*CONS[15], dro*CONS[16]);
	  le.Pop();
	  lpose.Pop();
	}
      }
  }
}


void CercarHypo(Taxonomia & t, node n, list<node_pes> & l, float dr)
{
  node f;
  node_pes ln;
  list_item it;

  forall_adj_nodes(f, n)
  {
    ln.n = f;
    ln.dr = dr;
    if((it = l.search(ln))){
      if(dr > l[it].dr){
	l[it].dr = dr;
	CercarHypo(t, f, l, dr*CONS[16]);
      }
    }
    else{
      l.append(ln);
      CercarHypo(t, f, l, dr*CONS[16]);
    }
  }
  
}

class syn_pes{
public:
  long syn;
  float dr, dre, dro;
  friend istream& operator >> (istream& is, syn_pes &n){return is;};
  friend ostream& operator << (ostream& os, const syn_pes &n){
    os<<n.syn<<" "<<n.dr<<" "<<n.dre<<" "<<n.dro<<" // ";
    return os;
  };
  friend int compare(const syn_pes & e1, const syn_pes & e2){return leda::compare(e1.syn, e2.syn);}
  friend bool operator == (const syn_pes & e1, const syn_pes & e2){return e1.syn==e2.syn;};

};

void CercarHypeWN(long s, list<syn_pes> &l, float dr)
{
  LlistaSynset ls;
  syn_pes sp;
  list_item it;

  ls.hype(s);
if(DEBUG)cerr<<"\nLa llista syn: "<<s<<" es "<<ls<<endl;
  forall(s, ls){
    sp.syn = s;
    sp.dr = dr;
    if((it = l.search(sp))){
      if(dr > l[it].dr){
	l[it].dr = dr;
	CercarHypeWN(s, l, dr*CONS[13]);
      }
    }
    else{
      l.append(sp);
      CercarHypeWN(s, l, dr*CONS[13]);
    }
  }
if(DEBUG)cerr<<"\nfi cercaWN";
};


void CercarHypeWN(long s, list<syn_pes> &l, float dre, float dro){
  LlistaSynset ls;
  syn_pes sp;
  list_item it;

  if(dre > LDRE){
    ls.hype(s);
   forall(s, ls){
      sp.syn = s;
      sp.dre = dre; sp.dro =dro;
      if((it = l.search(sp))){
	if(dre > l[it].dre || dro > l[it].dro){
	  l[it].dre = dre;
	  l[it].dro = dro;
	  CercarHypeWN(s, l, dre*CONS[13], dro*CONS[14]);
	}
      }else{
	l.append(sp);
	CercarHypeWN(s, l, dre*CONS[13], dro*CONS[14]);
      }
    }
  }
};



void CercarHypoWN(long s, list<syn_pes> &l, float dr)
{
  LlistaSynset ls;
  syn_pes sp;
  list_item it;

  ls.hypo(s);
  forall(s, ls){
    sp.syn = s;
    sp.dr = dr;
    if((it = l.search(sp))){
      if(dr > l[it].dr){
	l[it].dr = dr;
	CercarHypoWN(s, l, dr*CONS[14]);
      }
    }
    else{
      l.append(sp);
      CercarHypoWN(s, l, dr*CONS[14]);
    }
  }
}


class coincideix{
public:
  node n;
  int posEtiq;
  float dr;
  friend istream& operator >> (istream& is, coincideix &c){return is;};
  friend ostream& operator << (ostream& os, const coincideix &c){
    os<<c.n<<" "<<c.posEtiq<<" "<<c.dr<<" ";
    return os;
  };
  friend int compare(const coincideix & c1, const coincideix & c2){
    return leda::compare(c1.dr, c2.dr);
  }
};

void zeroFills(Taxonomia & t, node n, Etiqueta & e){
  //  Si el dos synsets tenen 0 fills
  LlistaSynset lsp;
  Conexio c;

  if(t.indeg(n) == 0){
    lsp.hypo(e.syn());
    if(lsp.empty()){
      c.buidar();
      c.pesconex = RENF;
      c.tipus = Altres;
      e.conectar(c);
    }
  }
}

void igualFills(Taxonomia & t, node n, Etiqueta & e){
  // Si el dos synsets tenen igual nombre de fills (19/12/2002)
  LlistaSynset lsp;
  Conexio c;

  lsp.hypo(e.syn());
  if(lsp.length() == t.indeg(n)){
    c.buidar();
    c.pesconex = RENF;
    c.tipus = Altres;
    e.conectar(c);
  }
}

void diferenciaFills(Taxonomia & t, node n, Etiqueta & e){
  // a m�s diferencia del nombre de fills menys pes (3/10/2003)
  LlistaSynset lsp;
  Conexio c;
  float diff;

  lsp.hypo(e.syn());
  diff = (lsp.length()+0.1)/(t.indeg(n)+0.1);
  if(diff > 1)diff = 1/diff;
  diff = pow(diff, 0.5);
  c.buidar();
  c.pesconex = RENF * diff;
  c.tipus = Altres;
  e.conectar(c);
}
#endif










