bool NOTERROROVERFLOW = true;

#include "hypepo.hh"
#include "/usr/include/math.h"
#include <LEDA/core/string.h>
#include <unistd.h>

using std::ofstream;
using std::ifstream;

using std::setw;
using std::setfill;
using std::endl;

//Funci� auxiliar que extreu i elimina la primera paraula del string.
string primer(string & s)
{
  int i = s.pos(" ");
  while(i==0){
    s = s.del(0);
    i = s.pos(" ");
  }
  if(i<0)i=s.length();
  string s1 = s(0, i-1);
  s = s.del(0, i);
  return s1;
}

//Escriu el misatge d'error i acaba l'execusi�.
void error(const char *c1, const char *c2 ){
  cerr <<"\nError: "<< c1 << c2 << endl;
  exit(1);
}

//Diu si un car�cter �s un digit o no.
bool digit(char d){
  return d>='0' && d<='9';
}


// Elimina el sufix '_N_N' de la paraula espanyola.
string mot(string s){
  int i = s.pos("_");
  while(i != -1 && !digit(s[i+1])) i = s.pos("_", i+1);
  if(i == -1) return s;
   else
     return s(0, i-1);
}

string diccFile(char * orig, char * dest, int pos){
  string spos = posString(pos);

  char kk[] = "fileXXXXXX";
  mkstemp(kk);
  string kk1 = kk;
  printf("%s\n", T_ORIG);
  string cmd = "export LC_ALL=C; cat "+ (string)PREFIXWN + T_ORIG + "/dict/data." + spos + " | egrep '^[0-9]' | gawk '{xn= strtonum(\"0x\"$4 ); for(i=0;i<xn;i++){t=5+2*i;gsub(\"-\", \"_\", $t); print tolower($t), $1}}' | sort |uniq | gawk 'BEGIN{pri=\"\"}{if(pri == $1) cad = cad \" \" $2;else{print pri, cad, \"#\"; cad = $2;pri = $1}}END{print pri, cad, \"#\"}' > " + kk1;
  DBG cmd<<endl;
  system(cmd);
  DBG "aqui"<<endl;
  if(!strcmp(T_ORIG, "sp")){
    strcpy(kk, "fileXXXXXX");
    mkstemp(kk);
    string aux = kk;
    cmd = "mv "+kk1+" "+aux;
    system(cmd);
    cmd = "export LC_ALL=C; join " + (string)PREFIXWN + T_ORIG + "/dict/data."+spos+".dicc "+aux+" |gawk '{for(m=1;$m != \"#\"; m++); for(i=2; i<m; i++)for(j=m+1; j<NF; j++)print $i, $j}' |sort |uniq |gawk '{if(ss == $1) cad = cad \" \" $2; else{print ss, cad, \"#\"; cad = $2; ss = $1}}END{print ss, cad, \"#\"}' > "+kk1;
    system(cmd);
  }
  DBG3 "orig: "<< cmd <<endl;

  strcpy(kk, "fileXXXXXX");
  mkstemp(kk);
  string kk2 = kk;
  cmd = "export LC_ALL=C; cat " + (string)PREFIXWN + T_DEST + "/dict/data." + spos +
        " | egrep '^[0-9]' | gawk '{xn= strtonum(\"0x\"$4 ); for(i=0;i<xn;i++){t=5+2*i;gsub(\"-\", \"_\", $t); print tolower($t), $1}}' | sort|uniq | gawk 'BEGIN{pri=\"\"}{if(pri == $1) cad = cad \" \" $2;else{print pri, cad; cad = $2;pri = $1}}END{print pri, cad}' > " + kk2;
  system(cmd);
  DBG3 "dest: "<< cmd <<endl;

  strcpy(kk, "fileXXXXXX");
  mkstemp(kk);
  string kk12 = kk;
  cmd = "export LC_ALL=C; join "+ kk1 + " " + kk2 + " > " + kk12;
  system(cmd);
  DBG3 "join: "<< cmd <<endl;

  strcpy(kk, "fileXXXXXX");
  mkstemp(kk);
  string dicc = kk;
  cmd = "export LC_ALL=C; cat " + kk12 + " | gawk '{for(m=1;$m != \"#\";m++); for(i=2;i<m;i++) for(j=m+1;j<=NF;j++)print $i, $j, \"1\"}' | sort | uniq > " + dicc;
  system(cmd);
  DBG3 "dict: "<< cmd <<endl;
  system("echo >>"+dicc);
  cmd = "rm " + kk1;
  system(cmd);
  cmd = "rm " + kk2;
  system(cmd);
  cmd = "rm " + kk12;
  system(cmd);

  return dicc;
}


string nodeFile(char * orig, int pos){
  char kk[] = "fileXXXXXX";
  mkstemp(kk);
  string fileNodes = kk;
  string cmd = "export LC_ALL=C; cut -d' ' -f1 "
               + (string)PREFIXWN + T_ORIG
               + "/dict/data." + posString(pos)
               + " | grep '^[0-9]' > " + fileNodes;
  system(cmd);
  system("echo \".\" >> "+fileNodes);
  return fileNodes;
}


string taxFile(string fn, int pos){
  char kk[] = "fileXXXXXX";
  mkstemp(kk);
  string fileTax = kk;
  ofstream ost;
  ost.open(fileTax);

  long syn, shype;

  ifstream isn;
  isn.open(fn);
  DBG2 "VERSIOWN: "<<VERSIOWN<<endl;
  ObrirWordNet();
  DBG2 "WordNet obert"<<endl;
  string l;
  LlistaSynset ll;
  l.read_line(isn); 

  while(l.length() != 1){
    syn = atol(l);
    DBG2 "synset1: "<<syn<<endl;
    ll.hype(syn, pos);
    DBG2 "synset2: "<<syn<<endl;
    forall(shype, ll){
      ost << l << " ";
      ost <<setiosflags(ios::right)<<setw(8)<<setfill('0')<<shype
	  <<resetiosflags(ios::right)<<endl;
    }
    ll.clear();
    l.read_line(isn);
  }
  DBG2 "En taxFile3 NITE: "<<NITE<<endl;
  ost<<endl;
  DBG2 "En taxFile4 NITE: "<<NITE<<endl;
  isn.close();
  ost.close();
  DBG2 "En taxFile5 NITE: "<<NITE<<endl;
  TancarWordNet();
  return fileTax;
}















