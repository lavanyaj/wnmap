#ifndef _ESTRUC_HH
#define _ESTRUC_HH

#include "info.hh"
#include <LEDA/core/string.h>
#include <LEDA/core/list.h>
#include <LEDA/core/d_array.h>
#include <LEDA/graph/graph.h>

#include <iostream>

using std::istream;
using std::ostream;
using std::endl;
using leda::string;
using leda::node;
using leda::edge;
using leda::list;
using leda::d_array;
using leda::GRAPH;


enum TipusEnllac {
  CapDalt, CapBaix, Ambdos, Altres
};

//////////////////////////////////////////////////////////////////
//                         TAD Conexio                          //
//////////////////////////////////////////////////////////////////

class direccio{
public:
  friend std::istream& operator>> (std::istream&, direccio&);
  friend std::ostream& operator << (std::ostream&, const direccio&);
  node dn;
  int posEtiq;
  int posNode;
  list<int> poslle;
  list<edge> lle;
};

class Conexio{
public:
  friend istream& operator >> (istream& is, Conexio &c);
  friend ostream& operator << (ostream& os, const Conexio &c);
  friend int compare(const Conexio &c1, const Conexio &c2);
  void conectar(direccio d) {llptre.append(d);}
  void buidar() {llptre.clear();}
  list<direccio> llptre;
  float  pesconex;
  TipusEnllac tipus;
};

class Conedge{
public:
  friend istream& operator >> (istream& is, Conedge &c);
  friend ostream& operator << (ostream& os, const Conedge &c);
  int etp, etf;
  node np, nf;
  int posnp, posnf;
  float pes;
};

//////////////////////////////////////////////////////////////////
//                        TAD Etiqueta                          //
//////////////////////////////////////////////////////////////////

class Etiqueta 
// son els items per la llista associada a cada element de la tax. origen
{
public:
  void assig(string);
   // un string format per: "<synset> <pes>"
   // ho assigna al corresponen element de la etiqueta.

  long syn() {return synset;}      // funcions per obtenir el valor de cada
  float pes() {return pes_enll;}   // element de la etiqueta.
  float pant() {return pes_ant;}
  
  void put_pes(float p) { pes_enll = p;}
              //Per modificar el pes de la etiqueta.
  void ac_pant() {pes_ant = pes_enll;}
             //Posa el pes de la etiqueta al pes anterior. 
  void conectar(Conexio);
  list<Conexio> & AccesConex() {return conexions;}
  friend int compare(const Etiqueta & e1, const Etiqueta & e2);
  friend istream& operator >> (istream& is, Etiqueta &e);
  friend ostream& operator << (ostream& os, const Etiqueta &e);
  friend bool operator == (const Etiqueta & e1, const Etiqueta & e2);
private:
  long synset;
  float pes_enll, pes_ant;
  list<Conexio> conexions;
};



//////////////////////////////////////////////////////////////////
//                     TAD LlistaEtiq                           //
//               Es una llista de etiquetes                     //
//////////////////////////////////////////////////////////////////


class LlistaEtiq: public list<Etiqueta>
// declara llista com una llista de etiquetes.
{
public:
  void normal();  
           //Normalitza els pessos de les etiquetes de manera que 
           //el sumatori per cada llista sigui 1.
};


//////////////////////////////////////////////////////////////////
//          TAD diccionari. Es un diccionari biling�e           //
// L'entrada un mot espanyol, contingut una llista d'etiquetes. //
//////////////////////////////////////////////////////////////////


class Diccionari: public d_array<string, LlistaEtiq>
// es una taula, l'index es un mot espanyol.
{
public:
 void llegir();   
           //Per llegix el diccionari de un fitxer de text en el
           // que cada linia t� el format: 
           // <mot espanyol> <synset> <pes> 
};

//////////////////////////////////////////////////////////////////
//                       TAD Contador                           //
//               Hi tenim tots els contadors                    //
//////////////////////////////////////////////////////////////////

typedef class Taxonomia;
void CrearTaulaDestUnic(Taxonomia & t);

class Comptador
{
public:
  Comptador(Taxonomia &);
  void IncActEtiq(LlistaEtiq &);
  void IncActEnllac(list<Conexio>);
  void incNrelax() {Nrelax++;}
  void incNtraduc() {Ntraduc++;}
  void incNmultit() {NmulTrac++;}
  void escriure();
private:
  long Nnodes,     //Nombre de nodes de la taxonom�a
       Netiq,      //Total d'etiquetes de tota la taxonom�a
       NenCapDalt, //Total d'enlla�os a hyperonim de totes les etiquetes
       NenCapBaix, //Total d'enlla�os a hyponims de totes les etiquetes
       NenAmbdos,  //Total d'enlla�os a hyper/hyponim de totes les etiq.
       Nrelax,     //Nombre de nodes amb mes d'una etiq. que hem aplicat la relaxaci�
       Ntraduc,    //Nombre de nodes que tenen traducci�
       NmulTrac,   //Nombre de nodes que tenen mes d'una traducci�
       MaxEtNode,  //Nombre m�xim d'etiq. d'un node
       MaxEnCapD,  //Nombre m�xim d'enlla�os a hyperonim d'una etiq.
       MaxEnCapB,  //Nombre m�xim d'enlla�os a hyponim d'una etiqueta
       MaxEnAm;    //Nombre m�xim d'enlla�os a hyper/hypo d'una etiq.
};


//////////////////////////////////////////////////////////////////
//                      TAD taxonomia                           //
//        Es una taxonom�a de paraules espanyoles,              //
//      cada paraula t� associada una llista de etiques.        //
//////////////////////////////////////////////////////////////////



struct pesos{
  friend istream& operator >> (istream& is, pesos &p){return is;};
  friend ostream& operator << (ostream& os, const pesos &p){return os;};
  float pes, pes_ant;
};



//Informaci� que tenim en cada node de la taxonom�a, exepte l'arrel
class infonode{
public:
  friend istream& operator >> (istream& is, infonode &i);
  friend ostream& operator << (ostream& os, const infonode &i);
 
  infoword info;          //mot de la taxonomia origen
  list<pesos> grups;
  LlistaEtiq ll;
  int nn;             //Numero del node
  short SuportNoZeroNode;
  float pesEtiqueta(Etiqueta e);
};

namespace leda {
 inline int compare(const infonode& i1, const infonode& i2)
{
 if(i1.nn < i2.nn)return -1;
  if(i1.nn > i2.nn)return 1;
  return 0;	
} 
};

struct infoedge{
  friend istream& operator >> (istream& is, infoedge &i);
  friend ostream& operator << (ostream& os, const infoedge &i);
  int ne, genus;
  float pes, pes_ant;
  list<Conedge> llcon;
};

class Taxonomia : public GRAPH<infonode, infoedge>{
public:
  void llegir();
      //Llegeix un fitxer de text que t� la taxonom�a i monta 
      //l'arbre, junt amb la llista de etiquetes correspones 
      //a la traducci� englesa.

  void normal();      
      //Normalitza totes les llistes de etiquetes.

  void escriu(); //Escriu el graf sense identar.

  void comptar();
      // Compta una serie de cantitats que estan a la clase Comptador

  void relax(char *, int = 0);
      // Funci� principal de la relaxaci�
private:
  void RelaxNormal(char *, int);
  void RelaxGen(char *, int);
  void ActualitzarLlistaNodes(list<node> &);
  void pes_a_pant();
  float suport(node, Etiqueta &);
  float suportedge(edge &);
  bool seguirelax(node);
  bool seguirelax(list<node> &);
  void treuNodes(list<node> &);
  void GuardarGraf(char*, int);
     // Funcions auxiliars per la relaxaci�
  void comptar(node, Comptador &);
     // Funcio auxiliar del anterior comptar()
};

//////////////////////////////////////////////////////////////////
//              TAD Auxiliar per la relaxaci�                   //
//Cada posicio guarda el suport de mateixa posici� de l'etiqueta//
//////////////////////////////////////////////////////////////////

class suportlist : public list<float>
  // Llista de suports associada a una llista de etiquetes.
{
public:
  void normal(); 
       //Normalitza la llista de suports al interval [-1, 1]
  float sumar();
};


#endif












