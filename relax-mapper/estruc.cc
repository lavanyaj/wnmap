#include "estruc.hh"
#include "funaux.hh"
#include "/usr/include/math.h"
#include <string.h>
#include <stdio.h>
#include <fstream.h>


extern d_array<long, list<infonode> > TaulaDestUnic;
using std::istream;
using std::ostream;
using std::endl;

using std::ios;
using std::cout;

using std::setiosflags;
using std::setprecision;
using std::setw;
using std::resetiosflags;

using leda::string;
using leda::node;
using leda::list;
using leda::list_item;

istream& operator >> (istream& is, TipusEnllac &t)
{
  int e;
  is >> e;
  switch(e){
    case 0:  t = CapDalt; break;
    case 1:  t = CapBaix; break;
    case 2:  t = Ambdos;  break;
  default :  t = (TipusEnllac)e;
  }
  return is;
}

/*****************************************************************
***   Implentaci� de les funcions associades a direccio        ***
*****************************************************************/
istream& operator >> (istream& is, direccio &d){
  is >> d.posNode >> d.posEtiq;
  d.poslle.read(is, '#');
  return is;
}

ostream& operator << (ostream& os, const direccio &d){
  os <<d.posNode<<" "<<d.posEtiq<<" "<<d.poslle<<'#';
  return os;
}

/*****************************************************************
***   Implentaci� de les funcions associades al TAD Conexio    ***
*****************************************************************/

istream& operator >> (istream& is, Conexio &c){
  is >> c.pesconex >>c.tipus;
  c.llptre.read(is, '#');
  return is;
}

ostream& operator << (ostream& os, const Conexio &c){
  os << c.pesconex <<" "<<c.tipus<<" "<< c.llptre<<'#';
  return os;
}
int compare(const Conexio &c1, const Conexio &c2){
  //Aquesta comparaci� est� al rav�s per que ordeni de gran a petit.
  return (int)((c2.pesconex - c1.pesconex)*1000); 
}
/***************************************************************
 *************************************************************/
istream& operator >> (istream& is, Conedge &c){
  is>>c.posnp>>c.etp>>c.posnf>>c.etf>>c.pes;
  return is;
}

ostream& operator << (ostream& os, const Conedge &c){
  os<<c.posnp<<" "<<c.etp<<" "<<c.posnf<<" "<<c.etf<<" "<<c.pes<<" ";
  return os;
}

istream& operator >> (istream& is, infoedge &i){
  is >>i.ne>>i.genus >> i.pes >> i.pes_ant;
  i.llcon.read(is, '#');
  return is;
}

ostream& operator << (ostream& os, const infoedge &i){
  os << i.ne <<" "<< i.genus <<" "<< i.pes <<" "<<i.pes_ant<<" "<< i.llcon<<'#';
  return os;
}

/*****************************************************************
***   Implentaci� de les funcions associades al TAD Etiqueta   ***
*****************************************************************/

void Etiqueta::assig(string s)
{
  synset = atol(primer(s));
  pes_ant = pes_enll = atoi(s);
}


void Etiqueta::conectar(Conexio c)
{
  conexions.append(c);
}

istream& operator>> (istream& is, Etiqueta &e)
{
  is >> e.synset >> e.pes_enll >> e.pes_ant;
  e.conexions.read(is, '#');
  return is;
};

ostream& operator<< (ostream& os, const Etiqueta &e)
{
  os <<e.synset <<" " <<e.pes_enll <<" " <<e.pes_ant<<" "<< e.conexions<<"#";
  return os;
};

int compare(const Etiqueta & e1, const Etiqueta & e2) 
	{
		return leda::compare(e1.synset, e2.synset);
	}

bool operator == (const Etiqueta & e1, const Etiqueta & e2)
{
    return e1.synset==e2.synset;
}
/*****************************************************************
Moved to inline leda functions in .hh

int compare(const Etiqueta & e1, const Etiqueta & e2)
{
  return compare(e1.synset, e2.synset);
}
*****************************************************************/

/*****************************************************************
***  Implentaci� de les funcions associades al TAD LlistaEtiq  ***
*****************************************************************/

void LlistaEtiq::normal()
//Normalitza els pessos de les etiquetes de manera que el sumatori
//per cada llista sigui 1.
{
  double s = 0.0;
  Etiqueta e;
  list_item it;

  forall(e, *this)  s += e.pes();

  forall_items(it, *this)
  {
    e = (*this)[it];
    e.put_pes((float)(e.pes() / s));
    this->assign(it, e);
  }

}


/*****************************************************************
***  Implentaci� de les funcions associades al TAD diccionari  ***
*****************************************************************/

void Diccionari::llegir()
{
  string l, p, dicc;
  Etiqueta e;

  dicc = diccFile(T_ORIG, T_DEST, POS);
  filebuf bufdicc;

  string miterror = "no es pot obrir el fitxer: ";
  if (!bufdicc.open(dicc, ios::in))
    error (miterror, dicc);
  istream is(&bufdicc);

  if(DEBUG)cout<<"Anem a llegir dicc: "<<dicc<<endl;
  l.read_line(is);
  while(l.length() != 0)
  {
    DBG "Hem llegit: "<<l<<" eof: "<<is.eof()<<endl;
    p = primer(l);
    e.assig(l);
    (*this)[p].append(e);
    l.read_line(is);
  }
  if(DEBUG)cout<<"Hem llegit dicc"<<endl;
  bufdicc.close();
  system("rm "+dicc);
  DBG "filedicc: "<< dicc <<endl;
}
/*****************************************************************
***  Implentaci� de les funcions associades a la struc info    ***
*****************************************************************/

istream& operator >> (istream& is, infonode &i)
{
  is >>i.nn >> i.info.sp >> i.info.acep;
  i.ll.read(is, '#');
  return is;
};

ostream& operator << (ostream& os, const infonode &i)
{
  os <<i.nn<<" " <<i.info.sp<<" "<<i.info.acep<<" "<<i.ll<<'#';
  return os;
};
float infonode::pesEtiqueta(Etiqueta e){
  list_item it;
  it = ll.search(e);
  return ll[it].pes();
}

/*****************************************************************
***  Implentaci� de les funcions associades al TAD Comptador   ***
*****************************************************************/
Comptador::Comptador(Taxonomia & t)
{
  Nnodes = t.number_of_nodes();
  Netiq = NenCapDalt = NenCapBaix = NenAmbdos = Nrelax = Ntraduc
        = NmulTrac = MaxEtNode = MaxEnCapD = MaxEnCapB = MaxEnAm = 0;
}

void Comptador::IncActEtiq(LlistaEtiq & ll)
{
  int t;
  t = ll.length();
  Netiq += t;
  if(t > MaxEtNode) MaxEtNode = t;
}

void Comptador::IncActEnllac(list<Conexio> ll)
{
  Conexio c;
  int cD = 0, cB = 0;
  forall(c, ll){
    if(c.tipus == CapDalt) cD++;
    if(c.tipus == CapBaix) cB++;
  }
  int cA = cD * cB;
  NenCapDalt += cD;
  NenCapBaix += cB;
  NenAmbdos  += cA;
  if(cD > MaxEnCapD) MaxEnCapD = cD;
  if(cB > MaxEnCapB) MaxEnCapB = cB;
  if(cA > MaxEnAm)   MaxEnAm   = cA;
}

void Comptador::escriure()
{
  cout <<"Nombre de Nodes:                     "<<Nnodes    <<endl
       <<"Nombre d'etiquetes:                  "<<Netiq     <<endl
       <<"Enlla�os a hyperonims:               "<<NenCapDalt<<endl
       <<"Enlla�os a hyponims:                 "<<NenCapBaix<<endl
       <<"Enlla�os a hyper/hypo:               "<<NenAmbdos <<endl
       <<"Nombre de relaxacions:               "<<Nrelax    <<endl
       <<"Nombre de traduccions:               "<<Ntraduc   <<endl
       <<"Nombre de multitraduccions:          "<<NmulTrac  <<endl
       <<"M�xim etiquetes/node:                "<<MaxEtNode <<endl
       <<"M�xim enlla�os(hyper)/etiqueta:      "<<MaxEnCapD <<endl
       <<"M�xim enlla�os(hypo)/etiqueta:       "<<MaxEnCapB <<endl
       <<"M�xim enlla�os(hyper/hypo)/etiqueta: "<<MaxEnAm   <<endl;
}

/*****************************************************************
***  Implentaci� de les funcions associades al TAD Taxonomia   ***
*****************************************************************/

/*****************************************************************
Moved to inline leda functions in .hh

int compare(infoword i1, infoword i2){
  if(compare(i1.sp, i2.sp) != 0) return compare(i1.sp, i2.sp);
  else return compare(i1.acep, i2.acep);
}
int compare(infonode i1, infonode i2){
  if(i1.nn < i2.nn)return -1;
  if(i1.nn > i2.nn)return 1;
  return 0;
}
*****************************************************************/
void Taxonomia::llegir()
{
  Diccionari dic;
  dic.llegir();

  string l;
  node n;
  infonode i;
  int ConNode = 1;
  int ConEdge = 1;
  int grup;
  pesos p;
  d_array<string, list<node> > taula_sp;
  d_array<infoword, node> taula_word;

  cout<< "Anem a llegir els nodes"<<endl;

  string fileNodes = nodeFile(T_ORIG, POS);
  ifstream isn;
  isn.open(fileNodes);
  DBG2 "fileNodes: "<<fileNodes<<endl;

    isn >> l;     
  while(l.length() != 1){
    DBG3 "node: "<<l<<endl;
    i.nn = ConNode++;
    i.SuportNoZeroNode=0;
    i.info.sp = primer(l);
    if(l != "")i.info.acep = atoi(l); else i.info.acep = -1;
    i.ll=dic[i.info.sp];
    n = this->new_node(i);
    taula_sp[i.info.sp].append(n);
    taula_word[i.info]=n;
    isn >> l;     
   }
  isn.close();
  VERSIOWN = T_ORIG;
  string fileTax = taxFile(fileNodes, POS);
  system("rm "+fileNodes);
  ifstream ist;
  ist.open(fileTax);
  DBG2 "fileTax: "<<fileTax<<endl;

  cout<< "Anem a llegir les arestes"<<endl;
  infoword w;
  node ntarget;
  list<node> ln;
  infoedge edv;
  edv.pes =1.0; edv.pes_ant =1.0;
  p.pes =1.0; p.pes_ant =1.0;
  l.read_line(ist);
  while(l.length() != 0){
    w.sp = primer(l);
    w.acep = -1;
    n = taula_word[w];
    grup = 0;
    DBG2 "word: "<<w.sp<<" "<<w.acep<<endl;
    while(l.length()>0){
      ln = taula_sp[primer(l)];
      forall(ntarget, ln){
	edv.ne = ConEdge++;
	edv.genus = grup;
	DBG "  grup:   "<<grup<<endl;
	this->new_edge(n, ntarget, edv);
      }
      (*this)[n].grups.append(p);
      grup++;
    }
    l.read_line(ist);
  }
  ist.close();
  system("rm "+fileTax);
  DBG1 "Hem llegit nodes i arestes"<<endl;
}


void Taxonomia::normal(){
  node n;
  list<edge> le, leg;
  double s, ss[10];
  list_item it;

  forall_nodes(n, *this)
  {
    DBG "Normalitzem: "<<(*this)[n].info.sp<<endl;
    (*this)[n].ll.normal();
    if(EDGES){
      le = this->out_edges(n);
      s = 0.0;
      if(RELAXGEN){
	for(int i=0; i<10;i++)ss[i]=0.0;
	forall_items(it, le) ss[(*this)[le[it]].genus] += this->inf(le[it]).pes;
	forall_items(it, le)
	  (*this)[le[it]].pes = (*this)[le[it]].pes / ss[(*this)[le[it]].genus];
	s = 0.0;
	forall_items(it, (*this)[n].grups) s += (*this)[n].grups[it].pes;
	forall_items(it, (*this)[n].grups) (*this)[n].grups[it].pes /= s;
      }else{
	s = 0.0;
	forall_items(it, le) s += this->inf(le[it]).pes;
	forall_items(it, le)
	  (*this)[le[it]].pes = (*this)[le[it]].pes / s;
      }
    }
  }
}

void Taxonomia::escriu(){
  node n, nt;
  char m;
  Etiqueta e;
  list<edge> le;
  edge ed;

  forall_nodes(n, (*this)){
    LlistaEtiq & ll = (*this)[n].ll;
    if(ll.size()==1) m ='u';else m='m';
    cout <<m<<' '<<(*this)[n].info.sp<<" "<<(*this)[n].info.acep << endl;
    forall(e, ll){
       cout << m<<' ';
       for(int i=0; i<12; i++)  cout << ' '; cout <<"-->  ";
       printf(" %08ld ", e.syn());
       cout <<setiosflags(ios::right)<<setw(12)<< setprecision(3)<< e.pes() <<resetiosflags(ios::right)<< endl;
     }
    if(EDGES){
      le = this->out_edges(n);
      forall(ed, le){
	if(le.size()==1) m='u';else m='m';
	cout << m<<' ';
	for(int i=0; i<12; i++)  cout << ' '; cout <<"//>  ";
	nt = this->target(ed);
	cout <<(*this)[nt].info.sp<<" "<<(*this)[nt].info.acep;
       cout <<setiosflags(ios::right)<<setw(12)<< setprecision(3)
	    << this->inf(ed).pes<<"\t"
	    <<(*this)[n].grups[
		  (*this)[n].grups.get_item(this->inf(ed).genus)
	      ].pes 
	    <<resetiosflags(ios::right)<< endl;
      }
    }
  }
}

void Taxonomia::comptar()
{
  node n;
  Comptador c(*this);
  forall_nodes(n, *this) comptar(n, c);
  c.escriure();
}

void Taxonomia::comptar(node n, Comptador & c)
{
  Etiqueta e;

  LlistaEtiq & ll =  (*this)[n].ll;
  c.IncActEtiq(ll);
  if( !ll.empty())
  {
     c.incNtraduc();
     forall(e, ll) c.IncActEnllac(e.AccesConex());
     if(ll.size()>1)
     {
        c.incNmultit();
        if(this->inf(n).SuportNoZeroNode) c.incNrelax();
     }
  }
}


/*****************************************************************
***        Implentaci� de la relaxaci� pel TAD Taxonomia       ***
*****************************************************************/
void Taxonomia::relax(char* fg, int cont)
{
  if(RELAXGEN) RelaxGen(fg, cont);
	  else RelaxNormal(fg, cont);
}

void Taxonomia::RelaxNormal(char* fg, int cont)
{
  node n;
  edge ed;
  list_item it;
  suportlist sl;
  float sup, aux;
  int pot = 1, l_ll;

  list<node> Lln = this->all_nodes();
  DBG "Longitud inicial: "<<Lln.length() <<endl;
  do
  {
    if(DU) CrearTaulaDestUnic(*this);
    l_ll = 0;
    this->pes_a_pant();
    forall(n, Lln)
    {
      l_ll++;
      DBG "Relexem etiquetes"<<endl;
      infonode & infnode = (*this)[n];
      LlistaEtiq & ll = infnode.ll;
      sl.clear();
      forall_items(it, ll){
        sup=suport(n, ll[it]);
        sl.append(sup);
        if(sup)infnode.SuportNoZeroNode=1; 
      }
      sl.normal();
      forall_items(it, ll){
	aux = ll[it].pant()*(1+sl.pop()); //actualitzar
	if(aux < ZERO) ll[it].put_pes(0); //actualitzar
	else ll[it].put_pes(aux); //actualitzar
      }
      DBG "Relexem arestes"<<endl;
      if(EDGES){
	sl.clear();
	forall_out_edges(ed, n){
	  sup = suportedge(ed);
	  sl.append(sup);
	}
	sl.normal();
	forall_out_edges(ed, n){
	  aux = (*this)[ed].pes_ant;
	  if(aux < 1.0e-10)(*this)[ed].pes = 0;
	  else (*this)[ed].pes = (*this)[ed].pes_ant * (1+sl.pop());
	}
      }
    }
    cont++;
    this->normal();
    if(strcmp(fg, "")) GuardarGraf(fg, cont);
    if(EFICIENT && cont % (10*pot) == 1){
      pot *= 2;
      treuNodes(Lln);
      DBG "Longitud: "<<Lln.length()<<" a "<<cont<<"  "; system("date");
    }
    if(cont % pot == 0)cout << "Hem fet la relax n. " << cont<<"/"<<NITE<<"/"<<l_ll<<endl;
  }
  while(seguirelax(Lln) && cont<NITE);
  cout << "Hem fet la relax final n. " << cont<<"/"<< NITE <<endl;
}

void Taxonomia::RelaxGen(char* fg, int cont){
  node n;
  edge ed;
  list_item it;
  suportlist sl, slg, tsl[10]; //Ull, una paraula no pot tenir mes de 10 genus.
  float sup;

  list<node> Lln = this->all_nodes();
  do
  {
    this->pes_a_pant();
    forall(n, Lln)
    {
      if(DEBUG)cout<<"Relexem etiquetes"<<endl;
      infonode & infnode = (*this)[n];
      LlistaEtiq & ll = infnode.ll;
      sl.clear();
      forall_items(it, ll){
        sup=suport(n, ll[it]);
        sl.append(sup);
        if(sup)infnode.SuportNoZeroNode=1; 
      }
      sl.normal();
      forall_items(it, ll)
	ll[it].put_pes(ll[it].pant()*(1+sl.pop())); //actualitzar

      if(DEBUG)cout<<"Relexem arestes"<<endl;
      if(EDGES){
	if(DEBUG)cout<<(*this)[n].info.sp<<" "<<(*this)[n].info.acep<<endl;
	sl.clear();
	forall_out_edges(ed, n){
	  sup = suportedge(ed);
	  tsl[(*this)[ed].genus].append(sup);
	}
	for(int i=0; i<10 && !tsl[i].empty(); i++)slg.append(tsl[i].sumar());
	for(int i=0;i<10;i++){if(DEBUG)cout<<tsl[i]<<"$$"<<endl;tsl[i].normal();}
	forall_out_edges(ed, n)
	  (*this)[ed].pes = (*this)[ed].pes_ant * (1+tsl[(*this)[ed].genus].pop());
	if(DEBUG)cout<<slg<<"SLG"<<endl;
	slg.normal();
	forall_items(it, (*this)[n].grups)
	  (*this)[n].grups[it].pes = (*this)[n].grups[it].pes_ant * (1 + slg.pop());
      }
    }
    cont++;
    this->normal();
    if(strcmp(fg, "")) GuardarGraf(fg, cont);
    cout << "Hem fet la relax n. " << cont <<endl;
  }
  while(seguirelax(Lln) && cont<NITE);
}


void Taxonomia::pes_a_pant()
{
  node n;
  edge e;
  list_item it;

  forall_nodes(n, *this)
  {

    infonode & in = (*this)[n];
    forall_items(it, in.ll)
    {
      Etiqueta & e = in.ll[it];
      e.ac_pant();
    }
    forall_items(it, in.grups)
      in.grups[it].pes_ant = in.grups[it].pes;
  }
  if(EDGES)
    forall_edges(e, *this)
      (*this)[e].pes_ant = (*this)[e].pes;
}


float Taxonomia::suport(node n, Etiqueta & e)
{
   float sup = 0.0, supaux, pedge;
   list<Conexio> llc;
   Conexio c;
   direccio d;
   list_item itll, itptre, itlle;

   llc = e.AccesConex();
   forall_items(itll, llc){
     c = llc[itll];
     supaux = c.pesconex;
     forall_items(itptre, c.llptre){
       d = c.llptre[itptre];
       pedge=1.0; forall_items(itlle, d.lle) pedge *= (*this)[d.lle[itlle]].pes_ant;
       supaux *= (*this)[d.dn].ll[(*this)[d.dn].ll[d.posEtiq]].pant() * pedge;
     }
     sup += supaux;
   }
   if(DU){
     list<infonode> llifn = TaulaDestUnic[e.syn()];
     float act=0, sum = 0.0;
     infonode i, a;
     a = (*this)[n];
     forall(i, llifn){
       sum = sum + i.pesEtiqueta(e);
       if(i.nn == a.nn) act = i.pesEtiqueta(e);
     }
     if(sum != 0)sup += DU * act/sum;
     DBG i.info.sp<<"("<<e.syn()<<")  "<<act<<"  "<<sum<<endl;
   }
   return sup;
}

float Taxonomia::suportedge(edge & ed){
  float sup = 0.0;
  Conedge ce;

  forall(ce, (*this)[ed].llcon)
    sup += ce.pes * (*this)[ce.np].ll[(*this)[ce.np].ll.get_item(ce.etp)].pant()
                  * (*this)[ce.nf].ll[(*this)[ce.nf].ll.get_item(ce.etf)].pant();
  return sup;
}

bool Taxonomia::seguirelax(node n)
{
  Etiqueta e;
  bool seg=0;
  node nf;

  LlistaEtiq ll = this->inf(n).ll;
  list_item it;

  it = ll.first();
  while(it && !seg){
    e = ll[it];
    seg = fabs(e.pes()-e.pant()) > EPSILON;
    it = ll.succ(it);
  }
  if(seg) return 1;
   else
   {
     list<node> ln = this->adj_nodes(n);
     it = ln.first();
     while(it && !seg){
       nf = ln[it];
       seg = seguirelax(nf);
       it = ln.succ(it);
     }
     return seg;
   }
}


bool Taxonomia::seguirelax(list<node> & Ll)
{
  Etiqueta e;
  bool seg=0;
  list_item it, itn;

  itn = Ll.first();
  while(itn && !seg){
    LlistaEtiq & ll = (*this)[(Ll[itn])].ll;
    it = ll.first();
    while(it && !seg){
      e = ll[it];
      seg = fabs(e.pes()-e.pant()) > EPSILON;
      it = ll.succ(it);
    }
    itn = Ll.succ(itn);
  }
  return seg;
}
void Taxonomia::treuNodes(list<node> & Ll){
  list_item it, itn, aux;
  
  itn = Ll.first();
  while(itn){
    LlistaEtiq & ll = (*this)[(Ll[itn])].ll;
    it = ll.first();
    while(it && (ll[it].pes() == 0 || ll[it].pes() == 1))it = ll.succ(it);
    aux = itn;
    itn = Ll.succ(itn);
    if(! it)Ll.erase(aux);
  }
}


void Taxonomia::ActualitzarLlistaNodes(list<node> & Lln)
{
  list_item itn, ite;
  bool seg = true;

  forall_items(itn, Lln){
    node & n = Lln[itn];
    LlistaEtiq & Lle = (*this)[n].ll;
    ite = Lle.first();
    while(ite && seg){
      Etiqueta & e = Lle[ite];
      seg = fabs(e.pes()-e.pant()) < EPSILON;
      ite = Lle.succ(ite);
    }
    if(seg) Lln.erase(itn);
  }
} 
    

void Taxonomia::GuardarGraf(char* fg, int cont)
{
  int paritat = cont % 2;
  char charsufix = (char)(paritat + (int)'0');
  char sufix[] = {'.', charsufix, '\0'};
  strcat(fg, sufix);

  filebuf bufgraf;

  if (!bufgraf.open(fg, ios::out))
    error ("no es pot obrir el fitxer ", fg);
  ostream osgraf(&bufgraf);
  osgraf << cont << endl;
  osgraf << (*this);
  bufgraf.close();

  int l = strlen(fg)-2;
  fg[l] = '\0';
}


/*****************************************************************
***     Implentaci� de les funcions associades a suportlist    ***
*****************************************************************/

void suportlist::normal()
{
  list_item it, itmin, itmax;
  float min, max, dem, nor;

if(! this->empty())
  {
    itmin = this->min();
    itmax = this->max();
  
    min = (*this)[itmin];
    max = (*this)[itmax];
    if(min <0) min = -min;
    if(min >= max) dem = min; else dem = max;
    forall_items(it, *this)
    {
      if(dem != 0) nor = (*this)[it]/dem ; else nor = 0;
      this->assign(it, nor);
    }
  }
}

float suportlist::sumar(){
  float e, s = 0.0;

  forall(e, *this)s += e;
  return s;
}





