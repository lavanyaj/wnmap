#ifndef _FUNAUX_HH
#define _FUNAUX_HH
#include <LEDA/core/string.h>

using leda::string;

extern unsigned char DEBUG;
extern float EPSILON, ZERO;
extern float CONS[18];
extern int NITE;
extern bool DOBLES;
extern int PROFUN, DU, RW, RG, RA, RHM, FILLS, RENF;
extern long TREST;
extern float LDRE;
extern bool RELAXGEN, EFICIENT;
extern bool ONLYGRAF;
extern int HEURISTICA;
extern bool EDGES, FWG;
extern int POS;
extern char * PREFIXWN;
extern char * VERSIOWN;
extern char T_ORIG[40];
extern char T_DEST[40];


string primer(string & s);
void   error(const char *, const char * ="");
bool   digit(char d);
string mot(string s);
float CastSegur(double x);
void DeleteSignes(string & s);
string diccFile(char * orig, char * dest, int pos);
string nodeFile(char * orig, int pos);
string taxFile(string fn, int pos);

#define DBG if(DEBUG & 0x8)cout<<
#define DBG1 if(DEBUG & 0x1)cout<<
#define DBG2 if(DEBUG & 0x2)cout<<
#define DBG3 if(DEBUG & 0x4)cout<<

#endif
